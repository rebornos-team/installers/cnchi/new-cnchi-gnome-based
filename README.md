# new cnchi gnome based

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

<p align="center">
<img src="https://gitlab.com/rebornos-team/installers/cnchi/new-cnchi-gnome-based/-/raw/master/Screenshot-20210630-1.png">
</p>

Dependencies required to create the ISO:

```
sudo pacman -S archiso mkinitcpio-archiso mkinitcpio-nfs-utils squashfs-tools git
```

cnchi has been updated to work with the latest version of archiso.

This ISO comes with Bluetooth active by default.

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/installers/cnchi/new-cnchi-gnome-based.git && cd new-cnchi-gnome-based
```

Dependencies:

```
sudo pacman -S archiso mkinitcpio-archiso mkinitcpio-nfs-utils squashfs-tools git
```

How to build the installer:

```
sudo ./fix_permissions.sh
sudo ./build.sh -v
```

The installer ISO will be in the **out** folder (folder that will be created automatically).

This installer downloads the cnchi code from the repository.

